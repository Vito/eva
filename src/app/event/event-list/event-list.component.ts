import { Component, OnInit } from '@angular/core';
import { flatbuffers } from 'flatbuffers';
import { eva } from '../../shared/fbs/user-event_generated';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  events: eva.fbs.UserEvent[] = [];

  constructor() {
      const builder = new flatbuffers.Builder();
      eva.fbs.UserEvent.startUserEvent(builder);
      const serialized = eva.fbs.UserEvent.endUserEvent(builder);
  }

  ngOnInit(): void {
  }

}
