import { Component, OnInit } from '@angular/core';
import { flatbuffers } from 'flatbuffers';
import { eva as ue } from '../../shared/fbs/user-event_generated';
import { eva as e } from '../../shared/fbs/event_generated';
import { eva as u } from '../../shared/fbs/user_generated';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

      const builder = new flatbuffers.Builder(0);

      // create location
      let lat:number = 44.23;
      let lng:number = 23.12;
      let country = builder.createString("AT");
      let zip = builder.createString("6971");
      let city = builder.createString("Hard");
      let street = builder.createString("Pfaenderweg 8");
      let desc = builder.createString("Waldstadion Hard");

      e.fbs.Location.startLocation(builder);
      e.fbs.Location.addLat(builder, lat);
      e.fbs.Location.addLng(builder, lng);
      e.fbs.Location.addCountry(builder, country);
      e.fbs.Location.addZip(builder, zip);
      e.fbs.Location.addCity(builder, city);
      e.fbs.Location.addStreet(builder, street);
      e.fbs.Location.addDesc(builder, desc);

      let location = e.fbs.Location.endLocation(builder);

      // create event
      let id:number = 12341324;
      let status = e.fbs.EventStatus.Pending;
      let name = builder.createString("Training");
      let eventDesc = builder.createString("warm anziehen... es wird kalt");
      let startTimestamp = Date.now();
      let endTimestamp = Date.now() + 1*3600;
      let nrParticipantsConfirmed = 13;
      let nrParticipantsCanceled = 3;

      e.fbs.Event.startEvent(builder);
      e.fbs.Event.addId(builder, id);
      e.fbs.Event.addStatus(builder, status);
      e.fbs.Event.addName(builder, name);
      e.fbs.Event.addDesc(builder, eventDesc);
      e.fbs.Event.addStartTimestamp(builder, startTimestamp);
      e.fbs.Event.addEndTimestamp(builder, endTimestamp);
      e.fbs.Event.addNrParticipantsConfirmed(builder, nrParticipantsConfirmed);
      e.fbs.Event.addNrParticipantsCanceled(builder, nrParticipantsCanceled);
      e.fbs.Event.addLocation(builder, location);

      let event = e.fbs.Event.endEvent(builder);

      // create user
      let userId:number = 1234;
      let userName = builder.createString("petar pan");

      let user = u.fbs.User.createUser(builder, userId, userName);


      // create UserEvent
      let userEvent = ue.fbs.UserEvent.createUserEvent(
          builder,
          user,
          event,
          ue.fbs.EventUserRole.Participant,
          ue.fbs.EventUserStatus.Confirmed);

    // create user event list
    let events = ue.fbs.UserEvents.createEventsVector(
        builder,
       [userEvent]);

    let userEvents = ue.fbs.UserEvents.createUserEvents(builder, events);

    builder.finish(userEvents);

    let userEventsObj:ue.fbs.UserEvents = ue.fbs.UserEvents.getRootAsUserEvents(builder.dataBuffer());


    for (let i = 0; i < userEventsObj.eventsLength(); i++)
    {
        let event0 = userEventsObj.events(i);
        console.log(event0.user().id());
        console.log(event0.user().name());
        console.log(event0.userRole().valueOf());
        console.log(event0.userStatus().valueOf());
        console.log(event0.event().id());
        console.log(event0.event().name());
        console.log(event0.event().status().valueOf());
        console.log(event0.event().startTimestamp());
        console.log(event0.event().endTimestamp());
        console.log(event0.event().location().country());
        console.log(event0.event().location().zip());
        console.log(event0.event().location().city());
        console.log(event0.event().location().street());
        console.log(event0.event().location().desc());
    }
    
  }
}
