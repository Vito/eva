cmake_minimum_required (VERSION 3.16)

project(eva_server)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(SRCS eva_server.cpp)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_executable(eva_server ${SRCS})
target_link_libraries(eva_server ${CONAN_LIBS})
