#include <iostream>
#include <string_view>
#include <vector>
#include <thread>
#include <algorithm>
#include <sstream>
#include "PerMessageDeflate.h"
#include "App.h"
using namespace std;

int main(int argc, const char* argv[])
{

    auto startApp = []()
    {
        struct PerSocketData {}; 

        uWS::App()
            .ws<PerSocketData>("/*", {
                    .compression = uWS::SHARED_COMPRESSOR,
                    .maxPayloadLength = 16 * 1024,
                    .idleTimeout = 10,
                    .maxBackpressure = 1 * 1024 * 1024,
                    .open = 
                        [](auto* ws)
                        {
                            cout << "Server: conneciton opened\n";
                        },
                    .message =
                        [](auto* ws, string_view message, uWS::OpCode opCode)
                        {
                            ws->send(message, opCode, true);
                        },
                    .drain =
                         [](auto* ws) {},
                    .ping = 
                        [](auto* ws) {},
                    .pong =
                        [](auto* ws) {},
                    .close =
                        [](auto* ws, int code, string_view message) {}
                })
            .get("/*",
                    [](auto* res, auto* req)
                    {
                        stringstream _;
                        _ << "server responding from thread: " << this_thread::get_id() << endl;
                        res->end(_.str());
                    })
            .listen(9001,
                    [](auto* token)
                    {
                        if (token)
                        {
                            cout << "Thread: " << this_thread::get_id() <<  " listening on port " << 9001 << endl;
                        }
                        else
                        {
                            cout << "Thread: " << this_thread::get_id() << " failed to listen on port " << 9001 << endl;
                        }
                    })
            .run();
    };

    vector<thread*> threads(thread::hardware_concurrency());

    transform(begin(threads), end(threads), begin(threads),
            [&startApp](thread* t) {
                return new thread(ref(startApp));
            });

    for (auto t : threads)
        t->join();
}
